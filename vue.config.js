process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: `
                    @import "@/scss/reset.scss";
                    @import "@/scss/globals.scss";
                `,
      },
    },
  },
};
