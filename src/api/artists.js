import axios from "axios";

const accessTokenLS = JSON.parse(sessionStorage.getItem("accessToken"));
const tokenTypeLS = sessionStorage.getItem("tokenType");

export const artist1 = axios(
  `https://api.spotify.com/v1/artists/6CvenAZJCVdFOBWGArUGUH/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
export const artist2 = axios(
  `https://api.spotify.com/v1/artists/2ybBRXuLdkekwNT4LzaowV/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
export const artist3 = axios(
  `https://api.spotify.com/v1/artists/2GSf9zWFG1UqZFqXZb9nzl/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
export const artist4 = axios(
  `https://api.spotify.com/v1/artists/4h0KpsfJhNPfo1TlhE4H9p/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
export const artist5 = axios(
  `https://api.spotify.com/v1/artists/5eeuv6wjOyBdN4Db2Z8p6C/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
export const artist6 = axios(
  `https://api.spotify.com/v1/artists/3Q4D4QMqdh4YmAD14xqESr/albums`,
  {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }
);
