import axios from "axios";
import {
  artist1,
  artist2,
  artist3,
  artist4,
  artist5,
  artist6,
} from "../api/artists";

const accessTokenLS = JSON.parse(sessionStorage.getItem("accessToken"));
const tokenTypeLS = sessionStorage.getItem("tokenType");

export const getArtists = ({ commit }) => {
  let artistsArray = [];
  Promise.all([artist1, artist2, artist3, artist4, artist5, artist6]).then(
    (responses) => {
      responses.forEach((response) => {
        artistsArray.push(response.data);
      });
    }
  );
  commit("SET_ARTISTS", artistsArray);
};

export const getArtist = ({ commit }, artistId) => {
  let url = `https://api.spotify.com/v1/artists/${artistId}/albums`;
  axios(url, {
    headers: {
      Authorization: `${tokenTypeLS} ${accessTokenLS}`,
    },
  }).then((response) => {
    commit("SET_ARTIST", response.data);
  });
};

export const addArtistToFavs = ({ commit }, { artist }) => {
  commit("ADD_TO_FAVS", { artist });
};

export const saveData = ({ commit }) => {
  commit("SAVE_DATA");
};

export const removeArtistFromFavs = ({ commit }, fav) => {
  commit("REMOVE_ARTIST", fav);
};

export const removeAll = ({ commit }) => {
  commit("REMOVE_ALL");
};
