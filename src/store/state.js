let favourites = window.localStorage.getItem("FAVS");

export default {
  artists: [],
  artist: null,
  favourites: favourites ? JSON.parse(favourites) : [],
};
