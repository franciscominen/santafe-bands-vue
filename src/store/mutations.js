export const SET_ARTISTS = (state, artists) => {
  state.artists = artists;
};

export const SET_ARTIST = (state, artist) => {
  state.artist = artist;
};

export const ADD_TO_FAVS = (state, { artist }) => {
  let artistInFavs = state.favourites.find((item) => {
    return item.artist.items[0].artists[0].id === artist.items[0].artists[0].id;
  });
  if (artistInFavs) {
    return null;
  } else {
    state.favourites.push({ artist });
  }
  window.localStorage.setItem("FAVS", JSON.stringify(state.favourites));
};

export const REMOVE_ARTIST = (state, fav) => {
  state.favourites = state.favourites.filter((item) => {
    return item.artist.items[0].artists[0].id !== fav.items[0].artists[0].id;
  });
  window.localStorage.setItem("FAVS", JSON.stringify(state.favourites));
};

export const REMOVE_ALL = (state) => {
  state.favourites = [];
  localStorage.removeItem("FAVS");
};
