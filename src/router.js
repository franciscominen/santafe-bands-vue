import Vue from "vue";
import Router from "vue-router";
import Meta from 'vue-meta'
import Home from "./components/layout/home/Home.vue";
/* import About from "./components/layout/about/About.vue"; */
Vue.use(Router);
Vue.use(Meta);

export default new Router({
  routes: [
    { path: "", component: Home, name: "home" },
    {
      name: "about",
      path: "/about",
      component: () =>
        import(
          /* webpackChunkName: "About" */ "./components/layout/about/About.vue"
        ),
    },
    {
      name: "Bands",
      path: "/bands",
      component: () =>
        import(
          /* webpackChunkName: "BandsContainer" */ "./components/containers/bands/bands-container/BandsContainer.vue"
        ),
    },
    {
      name: "BandDetail",
      path: "/band/:id",
      component: () =>
        import(
          /* webpackChunkName: "BandDetail" */ "./components/containers/bands/band-detail/BandDetail.vue"
        ),
    },
    {
      name: "Favourites",
      path: "/favourites",
      component: () =>
        import(
          /* webpackChunkName: "BandDetail" */ "./components/containers/favs/favs-container/FavsContainer.vue"
        ),
    },
    {
      name: "Playlists",
      path: "/playlists",
      component: () =>
        import(
          /* webpackChunkName: "BandDetail" */ "./components/containers/playlist/PlaylistsContainer.vue"
        ),
    },
  ],
});
